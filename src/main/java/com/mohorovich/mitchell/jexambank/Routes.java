/*
 */
package com.mohorovich.mitchell.jexambank;

import static spark.Spark.*;

/**
 *
 * @author MohorovichM
 */
public class Routes {
  public static void main(String[] args) {
    get("/", (request, response) -> "Hello World!");
  }
}
